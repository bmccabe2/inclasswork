#ifndef COUNTER_H
#define COUNTER_H

class Counter {

public:

  typedef enum { BIN, BCD } mode_t ;

  Counter ( ) ;
  Counter ( const Counter& other ) ;
  Counter ( unsigned count ) ;
  virtual ~Counter ( ) ;
  void set_count ( unsigned count ) ;
  unsigned get_count ( void ) const ;
  virtual void do_count ( ) ;
  void set ( unsigned count ) ;
  void set ( mode_t mode ) ;
  void set ( unsigned count, mode_t mode ) ;
  mode_t get_mode ( void ) const ;
  static unsigned get_number ( void ) ;
  Counter& operator= ( const Counter& other ) ;
  Counter& operator= ( unsigned count ) ;

protected:

  unsigned count ;
  unsigned& value ;
  static unsigned number ;
  mode_t mode ;

};

#endif